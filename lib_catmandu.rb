require "open3"
require "yaml"

class Catmandu
  def initialize(config_filename = "koha-tools-config.yaml")
    config = YAML.load(File.read(config_filename))
    @url_in = config["url_in"]
    @sls_query = config["sls"].map { |sls| "pica.sls=#{sls}" }.join(" or ")
  end

  def download(query = nil)
    if query
      full_query = "(#{query}) and (#{@sls_query})"
    else
      full_query = @sls_query
    end
    out, err, stat = Open3.capture3("catmandu", "convert", "SRU", "--base", @url_in,
                                    "--query", full_query,
                                    "--recordSchema", "marcxml", "--parser", "marcxml",
                                    "to", "MARC", "--type", "XML")
    if ! stat.success? || out.empty? || out == "</marc:collection>"
      puts "warning: catmandu problems"
      puts "catmandu error message: #{err}" unless err.empty?
      return nil
    end
    out
  end
end
