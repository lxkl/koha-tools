require "nokogiri"
require "yaml"

class KohaXML
  MARC21NS_URL = "http://www.loc.gov/MARC21/slim"
  MARC21NS = {"marc21" => MARC21NS_URL}

  def initialize(config_filename = "koha-tools-config.yaml")
    @outdoc = Nokogiri::XML::Document.new
    @root = Nokogiri::XML::Node.new("marc:collection", @outdoc)
    @root["xmlns:marc"] = MARC21NS_URL
    @outdoc.add_child(@root)
    @count_added_records = 0
    @count_added_copies = 0
    @last_resort_copy_records = []
    config = YAML.load(File.read(config_filename))
    @branchcode_in = config["branchcode_in"]
    @branchcode_out = config["branchcode_out"]
    @primary_barcode_regexp = to_regexp(config["primary_barcode_regexp"])
    @primary_category_regexp = to_regexp(config["primary_category_regexp"])
    @primary_itemtype_map = config["primary_itemtype_map"]
    @primary_zfield_map = config["primary_zfield_map"]
    @blacklist_regexp = to_regexp(config["blacklist_regexp"])
    @secondary_barcode_regexp = to_regexp(config["secondary_barcode_regexp"])
    @secondary_category_regexp = to_regexp(config["secondary_category_regexp"])
    @secondary_itemtype_map = config["secondary_itemtype_map"]
    @secondary_zfield_map = config["secondary_zfield_map"]
    @location_regexp = to_regexp(config["location_regexp"])
    @location_category_regexp = to_regexp(config["location_category_regexp"])
    @location_itemtype_map = config["location_itemtype_map"]
    @branchcode_in_regexp = to_regexp(config["branchcode_in_regexp"])
    @default_itemtype = config["default_itemtype"]
    @zfield_spec = config["zfield_string"].map do |str|
      re = Regexp.new(str.downcase.gsub(/\p{P}/, "\\p{P}").gsub(/\s+/, "\\s*"))
      [re, str]
    end
  end

  def parse(input_string)
    indoc = Nokogiri::XML.parse(input_string) do |config|
      config.strict
    end
    indoc.xpath("//marc21:record", MARC21NS).each do |record|
      do_add_record = false
      has_last_resort_copy = false
      barcode_list = []
      record.xpath("marc21:datafield[@tag=924]", MARC21NS).each do |copy|
        if copy.xpath("marc21:subfield[@code='b']", MARC21NS).any? { |x| x.content == @branchcode_in }
          df = Nokogiri::XML::Node.new("datafield", record)
          df["tag"] = "952"
          df["ind1"] = "0"
          df["ind2"] = " "
          addsf(df, "a", @branchcode_out)
          addsf(df, "b", @branchcode_out)
          g_subfields = getsf(copy, "g")
          barcode = nil
          itemtype = nil
          zfield = nil
          if x = standard_match(@primary_barcode_regexp, @primary_category_regexp,
                                @primary_itemtype_map, @primary_zfield_map, g_subfields)
            barcode, itemtype, zfield = x
          elsif ! first_match(@blacklist_regexp, g_subfields) && ! g_subfields.empty?
            if x = standard_match(@secondary_barcode_regexp, @secondary_category_regexp,
                                  @secondary_itemtype_map, @secondary_zfield_map, g_subfields)
              barcode, itemtype, zfield = x
            elsif (g_subfields.length == 2) && (x = first_match(@location_regexp, g_subfields))
              m, i = x
              barcode = g_subfields[1-i]
              zfield = m[:location]
              m, _ = first_match(@location_category_regexp, zfield)
              category = m[:category]
              itemtype = @location_itemtype_map[category]
            elsif first_match(@branchcode_in_regexp, getsf(copy, "h"))
              has_last_resort_copy = true
              barcode = g_subfields[0]
              itemtype = @default_itemtype
            end
          end
          puts "warning: got barcode but no itemtype" if barcode && ! itemtype
          puts "warning: got itemtype but no barcode" if itemtype && ! barcode
          next unless barcode && itemtype
          unless zfield
            @zfield_spec.each do |spec|
              if first_match(spec[0], g_subfields.map(&:downcase))
                zfield = spec[1]
                break
              end
            end
          end
          addsf(df, "p", barcode)
          addsf(df, "y", itemtype)
          addsf(df, "z", zfield) if zfield
          record.add_child(df)
          @count_added_copies += 1
          do_add_record = true
          barcode_list << barcode
        end
      end
      if do_add_record
        @root.add_child(record)
        @count_added_records += 1
      end
      if has_last_resort_copy
        s = ""
        s += barcode_list.join("\n") + "\n" unless barcode_list.empty?
        s += record.xpath("marc21:datafield", MARC21NS).map { |x| format_datafield(x) }.join("")
        @last_resort_copy_records << s
      end
    end
  end

  def write(outfilename)
    File.write(outfilename, @outdoc.to_xml(encoding: "UTF-8"))
  end

  def print_warnings(out = $stdout)
    unless @last_resort_copy_records.empty?
      out.puts "#{@last_resort_copy_records.length} record(s) containing last-resort copies:\n\n"
      out.puts @last_resort_copy_records.join("\n")
    end
  end

  def print_stats
    puts "records added: %5d" % @count_added_records
    puts " copies added: %5d" % @count_added_copies
  end

  private

  def standard_match(barcode_regexp, category_regexp, itemtype_map, zfield_map, g_subfields)
    if x = first_match(barcode_regexp, g_subfields)
      m, _ = x
      barcode = m[:barcode]
      m, _ = first_match(category_regexp, barcode)
      category = m[:category]
      itemtype = itemtype_map[category]
      zfield = zfield_map[category]
      return barcode, itemtype, zfield
    end
    return nil
  end

  def to_regexp(str_list)
    str_list.map { |x| Regexp.new(x) }
  end

  def first_match(re_list, str_list)
    re_list = Array(re_list)
    str_list = Array(str_list)
    str_list.each_with_index do |str, i|
      re_list.each do |re|
        res = re.match(str)
        return res, i if res
      end
    end
    nil
  end

  def addsf(df, code, content)
    sf = Nokogiri::XML::Node.new("subfield", df)
    sf["code"] = code
    sf.content = content
    df.add_child(sf)
  end

  def getsf(copy, code)
    copy.xpath("marc21:subfield[@code='#{code}']", MARC21NS).map(&:content)
  end

  def format_datafield(df)
    res = ""
    tag = df["tag"]
    if tag then tag = "%3d" % tag.to_i; else tag = "???" end
    ind1 = df["ind1"] || " "
    ind2 = df["ind2"] || " "
    is_first_line = true
    df.xpath("marc21:subfield", MARC21NS).each do |sf|
      code = sf["code"] || "?"
      if is_first_line
        res += "%s %s %s _%s%s\n" % [tag, ind1, ind2, code, sf.content]
        is_first_line = false
      else
        res += "        _%s%s\n" % [code, sf.content]
      end
    end
    res
  end
end
